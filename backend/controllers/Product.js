// import { decode } from "jsonwebtoken";
import Products from "../models/productModel.js";
import multer from "multer";


export const add_product = async (req, res) => {
	const storage = multer.diskStorage({
		destination:function (req, file, callback) {
			callback(null, './media') 
		}, 
			filename: function (req, file, callback) {
			callback(null, file.originalname)
		}
	})

	const upload = multer({
		storage: storage,
		fileFilter: function (req, file, callback) {
		  if (
			file.mimetype == "image/png" ||
			file.mimetype == "image/jpg" ||
			file.mimetype == "image/jpeg"
		  ) {
			return callback(null, true);
		  }
		  callback(null, false);
		  return callback(new Error("only png/jpg/jpeg format allowed"));
		},
		limits: {
		  // 8mb
		  fileSize: 8388608, //bytes
		},
	  });

	const uploadSingleImage = upload.single("image");

    uploadSingleImage(req, res, async function (err) {
		const {product_name, qty, price, image} = req.body

		if (err) {
			return res.json({ message: err.message });
		}
		console.log('bodyyy  ',req.body);
		console.log('name  ',req.body.product_name);
		try {
			await Products.create({
				user_id: req.user_id,
				product_name: product_name,
				qty: qty,
				price: price,
				image: req.file.path,
			})
				res.status(200).json({
				status: 200,
				message: "tambah produk berhasil",
				url: req.file.path,
				});
	
		} 	catch (error) {
				res.json({
					message: error.message
			})
		}
    });

	
}