import { Sequelize } from "sequelize";

const db = new Sequelize('auth_db', 'postgres', 'postgres',{
	host: "localhost",
	dialect: "postgres"
})

export default db