import { Sequelize } from "sequelize";
import db from "../config/Database.js";

const {DataTypes} = Sequelize;

const Products = db.define('products', {
	user_id:{
		type: DataTypes.INTEGER
	},
	product_name:{
		type: DataTypes.STRING
	},
	qty:{
		type: DataTypes.INTEGER
	},
	price:{
		type: DataTypes.INTEGER
	},
	image:{
		type: DataTypes.STRING
	}
},{
	freezeTableName: true
})

export default Products